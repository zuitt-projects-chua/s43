//alert("hello")

let posts = [];
let count = 1;

//Add post

document.querySelector('#form-add-post')
.addEventListener("submit", (event) => {
	//prevent default is 
	event.preventDefault()
	//default of submit to send data 
	//what are the values you will update
	posts.unshift({
		id: count,
		title: document.querySelector('#txt-title').value,
		body: document.querySelector('#txt-body').value
	})
	count++
	showPosts(posts)
	alert("Post successfully added")
})

const showPosts = (aposts) => {
	let postEntries = ""

	aposts.forEach((post) =>{
		postEntries += `

		<div id="post-${post.id}">
			<h3 id="post-title-${post.id}">${post.title}</h3>
			<p id="post-body-${post.id}">${post.body}</p>
			<button onclick="editPost('${post.id}')">Edit</button>
			<button onclick="deletePost('${post.id}')">Delete</button>
		</div>
		`;
	});

	document.querySelector('#div-post-entries').innerHTML = postEntries;
}

//Edit post function

const editPost = (id) => {


	document.getElementById("demo").style.display = "block";

	let title = document.querySelector(`#post-title-${id}`).innerHTML
	let body = document.querySelector(`#post-body-${id}`).innerHTML

	document.querySelector("#txt-edit-id").value = id 
	document.querySelector("#txt-edit-title").value = title
	document.querySelector("#txt-edit-body").value = body
}


// Update post

document.querySelector('#form-edit-post')
.addEventListener("submit", (event) => {

	document.getElementById("demo").style.display = "none";

	event.preventDefault();

	for(let i = 0 ; i < posts.length ; i++){
		if (posts[i].id.toString() === document.querySelector("#txt-edit-id").value){

			posts[i].title = document.querySelector("#txt-edit-title").value
			posts[i].body = document.querySelector("#txt-edit-body").value
			showPosts(posts)
			alert("Post successfully updated")

			break; 
		}
	}

})


// Activity delete post (all temporarily remove element not permanent dont know how )

const deletePost = (id) => {

	/* 
	
	document.getElementById(`post-title-${id}`).style.display = "none";
	document.getElementById(`post-body-${id}`).style.display = "none";
	document.getElementById(`post-${id}`).style.display = "none";
	*/

	
	
	document.querySelector(`#post-title-${id}`).remove();
	document.querySelector(`#post-body-${id}`).remove();
	document.querySelector(`#post-${id}`).remove();
	

	/*
	
	let deleted = null

	document.querySelector(`#post-${id}`).innerHTML = deleted;
	*/

	

}
                                                 


